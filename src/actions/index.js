import {
    GET_SUMMARY,
    GET_ACCSES_TOKEN,
    ACCESS__TOKEN
} from "../apis";

import { responseJson } from "../helpers/common.js";

/**
 * createAccessToken: Get Access Token
 */
export async function createAccessToken () {
    // const data = await fetch(GET_ACCSES_TOKEN, {
    //     method: 'POST',
    //     headers: {
    //         'Authorization': 'Basic Z28tY29yb25hLWFkbWluOjU1NzdZdnpVNGJLNjNhMVdJUTNaMDQzSA==',
    //         'Content-Type': 'application/json;charset=utf-8',
    //     },
    //     body: JSON.stringify({ 
    //         'Email':'stephgottsch@gmail.com', 
    //         'Subscription':'basic'
    //     })
    // });

    // return responseJson(data);
    return '5cf9dfd5-3449-485e-b5ae-70a60e997864';
}

/**
 * getSummary: Get Summary
 */
export async function getSummary () {
    const data = await fetch(GET_SUMMARY, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            // 'X-Access-Token': ACCESS__TOKEN
        }
    });

    return responseJson(data);
}
