import { getCookie } from "../helpers/cookie.js";

export const DOMAIN = 'https://api.covid19api.com/';
export const AUTH = 'auth/';

export const GET_SUMMARY = DOMAIN + 'summary';
export const GET_ACCSES_TOKEN =  DOMAIN + AUTH + 'access_token';
export const ACCESS__TOKEN = getCookie('accessToken');