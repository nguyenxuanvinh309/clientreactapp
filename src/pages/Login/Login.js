import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Image } from "semantic-ui-react";
import MangoLeftBanner from "../../assets/svg/Mango-logo-small.svg";
import threeDot from "../../assets/svg/gray-top-right-setting-dots.svg";
import { actionCreators } from "../../store/WeatherForecasts";
import PasswordKeyboardCheckIn from "../../components/PasswordKeyboardCheckIn/PasswordKeyboardCheckIn";
import Footer from "../../components/Footer/Footer";
import './Login.css';

class Login extends Component {
  componentDidMount() {
    // This method is called when the component is first added to the document
  }

  componentDidUpdate() {
    // This method is called when the route parameters change
  }

  ensureDataFetched() {}

  render() {
    return (
      <div className="loginPageContainer">
        <div className="loginPage-top">
          <div className="three-dot-icon">
            <div className="ui simple dropdown item">
              <i className="fa fa-users"></i>
              <Image
                src={threeDot}
                style={{
                  width: "30px",
                  height: "30px",
                }}
              ></Image>
              <i className="fa fa-caret-down"></i>
            </div>
          </div>
        </div>
        
        <div className="loginPage-body">
          <div className="logo-mango">
            {/* <Image src={MangoLeftBanner} className="mango-background" /> */}
          </div>
          <div className="login-title-text">
            <p>TECHNICIAN</p>
            <PasswordKeyboardCheckIn />
          </div>
        </div>
        {/* <div class="loginPage-bottom">
          <Footer />
        </div> */}
      </div>
    );
  }
}

export default connect(
  (state) => state.weatherForecasts,
  (dispatch) => bindActionCreators(actionCreators, dispatch)
)(Login);
