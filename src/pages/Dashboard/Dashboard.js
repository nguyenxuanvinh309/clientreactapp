import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Image } from "semantic-ui-react";
import MangoLeftBanner from "../../assets/svg/Mango-logo-small.svg";
import { actionCreators } from "../../store/WeatherForecasts";
import threeDot from "../../assets/svg/gray-top-right-setting-dots.svg";
import EnrichCoGrayLogoMedium from "../../assets/svg/Enrich-Co-gray-logo-medium.svg";
import { getSummary } from "../../actions";
import Loader from "../../components/Loader/Loader";
import Alert from "../../components/Alert/Alert";
import Cards from "../../components/Cards/Cards";
import { setAlertContext, openAlertContext } from "../../helpers/common.js";
import TableData from "../../components/TableData/TableData";
import "./Dashboard.css";
// import Chart from "../components/Chart/Chart";

// import styles from "./Dashboard.module.css"

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoaded: null,
      status: null
    };
  }

  componentDidMount() {
    // This method is called when the component is first added to the document
    /* Fetch Summary */
    this.fetchSummary();
  }

  componentDidUpdate() {
    // This method is called when the route parameters change
    this.closeStatusModal();
  }

  fetchSummary() {
    getSummary().then(res => {
      if (res === null) {
        this.setState({
          isLoaded: false,
          status: false,
          data: res
        });
      } else {
        this.setState({
          isLoaded: true,
          status: true,
          data: res
        });
        console.log(res)
      }
    });
  }

  closeStatusModal = () => {
    const { status } = this.state;

    if (status) {
      setTimeout(() => {
        this.setState({ status: null });
      }, 8000);
    }
  }

  render() {
    const { data, isLoaded, status } = this.state;
    const dataTable = data.Countries;
    
    return (
      <div className="dashboardPage-container">
        {/* <div className="dashboard-header">
          <div className="logo-mango-mini">
          </div>

          <div className="topBanner">
            <div style={{ width: "100%", display: "flex", marginTop: "10px" }}>
            </div>
          </div>

          <div className="three-dot-icon">
            <div className="ui simple dropdown item">
              <i className="fa fa-users"></i>
              <Image
                src={threeDot}
                style={{
                  width: "30px",
                  height: "30px",
                }}
              ></Image>
              <i className="fa fa-caret-down"></i>
            </div>
          </div>
        </div> */}

        {
          isLoaded ? (
            <div className="content-container">
              <Cards data={data} lastUpdate={data.Date} isloaded={isLoaded} />
              {/* <TableData dataTable={dataTable} /> */}
            </div>
          ) : <Loader />
        }
        
        { 
          //Set Alert Context
          setAlertContext(status) 
        }

        { 
          //Open Alert Context
          openAlertContext(status) 
        }
      </div>
    );
  }
}

export default connect(
  (state) => state.weatherForecasts,
  (dispatch) => bindActionCreators(actionCreators, dispatch)
)(Dashboard);