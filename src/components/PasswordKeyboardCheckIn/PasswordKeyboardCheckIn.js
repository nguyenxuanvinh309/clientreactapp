import React, { Component } from "react";
import { Input, Image } from "semantic-ui-react";
import button0 from "../../assets/svg/0.svg";
import button1 from "../../assets/svg/1.svg";
import button2 from "../../assets/svg/2.svg";
import button3 from "../../assets/svg/3.svg";
import button4 from "../../assets/svg/4.svg";
import button5 from "../../assets/svg/5.svg";
import button6 from "../../assets/svg/6.svg";
import button7 from "../../assets/svg/7.svg";
import button8 from "../../assets/svg/8.svg";
import button9 from "../../assets/svg/9.svg";
import buttonBack from "../../assets/svg/55px-back.svg";
import buttonC from "../../assets/svg/gray-x-circle.svg";
import { createAccessToken  } from "../../actions";
import Loader from "../Loader/Loader";

export default class PasswordKeyboardCheckIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      isLoaded: false,
    };
  }

  componentWillReceiveProps(nextProps) {}

  render() {
    const { isLoaded } = this.state;

    return (
      <div className="password-checkin-panel">
        <div className="password-input">
          <Input
            type="password"
            placeholder="Password"
            className="input10DigitPhone"
            readOnly
            id="password"
          />
        </div>

        <div className="row digitRow">
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("1");
            }}
          >
            <Image src={button1} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("2");
            }}
          >
            <Image src={button2} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("3");
            }}
          >
            <Image src={button3} className="no-zoom" />
          </div>
        </div>
        <div className="row digitRow">
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("4");
            }}
          >
            <Image src={button4} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("5");
            }}
          >
            <Image src={button5} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("6");
            }}
          >
            <Image src={button6} className="no-zoom" />
          </div>
        </div>
        <div className="row digitRow">
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("7");
            }}
          >
            <Image src={button7} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("8");
            }}
          >
            <Image src={button8} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("9");
            }}
          >
            <Image src={button9} className="no-zoom" />
          </div>
        </div>
        <div className="row digitRow">
          <div
            className="digitRow-div-special"
            onClick={() => {
              this.phonePadClearPassword();
            }}
          >
            <Image src={buttonC} className="no-zoom" />
          </div>
          <div
            className="digitRow-div"
            onClick={() => {
              this.phonePadPressed("0");
            }}
          >
            <Image src={button0} className="no-zoom" />
          </div>
          <div
            className="digitRow-div-special"
            onClick={() => {
              this.phonePadDeletePassword();
            }}
          >
            <Image src={buttonBack} className="no-zoom" />
          </div>
        </div>
      
        { isLoaded && <Loader /> }
      </div>
    );
  }

  /**
   * phonePadPressed: Press Input Password
   * @param {*} digit 
   */
  phonePadPressed(digit) {
    const { password } = this.state;

    if (password.length < 4) {
      // Update digit
      digit = password + digit;

      // Set state password
      this.setState({ password: digit });
      
      // Set Value to input
      document.getElementById('password').value = digit;

      if (digit.length === 4) {
        this.setState({ isLoaded: true });
        this.loginApp();      
      }
    }
  }

  loginApp = () => {
    // createAccessToken().then(res => {
    //   if (res === null) {
    //     document.cookie = `signin=${false}`;
    //   } else {
    //     document.cookie = `signin=${true}`;
    //     document.cookie = `accessToken=${res.Key}`;

    //     window.location.href = "/dashboard";

    //     //Set state for Loaded
    //     this.setState({ isLoaded: false });
    //   }
    // });

    createAccessToken().then(res => {
        if (res === null) {
          document.cookie = `signin=${false}`;
        } else {
          document.cookie = `signin=${true}`;
          document.cookie = `accessToken=${res}`;

          window.location.href = "/";

          //Set state for Loaded
          this.setState({ isLoaded: false });
        }
    });
  }
  
  /**
   * phonePadClearPassword: Clear Password 
   */
  phonePadClearPassword() {
    // Set state password
    this.setState({ password: '' });
    
    // Set Value to input
    document.getElementById('password').value = null;
  }
  
  /**
   * phonePadDeletePassword: Delete Password
   */
  phonePadDeletePassword() {
    let { password } = this.state;

    if (password.length > 0) {
      password = password.slice(0, -1);

      this.setState({ password: password });

      // Set Value to input
      document.getElementById('password').value = password;
    }
  }
}
