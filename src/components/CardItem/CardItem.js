import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Card, CardContent, Typography, Grid } from '@material-ui/core';
import CountUp from 'react-countup';

class CardItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data, lastUpdate, classGridName, name, description, nameData } = this.props;

        console.log(nameData);

        return (
            <Grid item xs={12} sm={4} md={4}>
                <CardContent className={`card ${classGridName}`}>
                    <Typography color="textSecondary" gutterBottom>
                        {name}
                    </Typography>
                    <Typography variant="h5" component="h2">
                        <CountUp start={0} end={data.Global[nameData]} duration={2.75} separator="," />
                    </Typography>
                    <Typography color="textSecondary">
                        {new Date(lastUpdate).toDateString()}
                    </Typography>
                    <Typography variant="body2" component="p">
                        {description}
                    </Typography>
                </CardContent>
            </Grid>
        );
    }
}

export default CardItem;