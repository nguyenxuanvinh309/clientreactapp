
import React, { Component, useMemo } from 'react';

import { useTable, useGlobalFilter, useFilters, usePagination } from 'react-table';
import GlobalFilter from "./GlobalFilter";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
});

function TableRender({ columnsTable, dataTable }) {
    const classes = useStyles();

    // Use the state and functions returned from useTable to build your UI
    const data = useMemo(() => dataTable, []);
    const columns = useMemo(() => columnsTable, []);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [pageCurrent, setPageCurrent] = React.useState(0);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        state,
        newPage,
        setGlobalFilter,
    } = useTable({
        columns,
        data,
    },
        useFilters,
        useGlobalFilter,
        usePagination
    )

    const handleChangePage = (event, newPage) => {
        setPageCurrent(newPage);
    };

    function handleChangeRowsPerPage (event) {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPageCurrent(0);
    };

    //This is state of GlobalFilter Component
    const { globalFilter } = state;
    
    // Render the UI for your table
    return (
        <div className="table-container">
            <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                     <TableHead>
                        {
                            headerGroups.map((headerGroup, i) => (
                                <TableRow key={i}>
                                    {
                                        headerGroup.headers.map((column, i) => (
                                            <TableCell key={i}>
                                                {column.render('Header')}
                                            </TableCell>
                                        ))
                                    }
                                </TableRow>
                            ))
                        }
                    </TableHead>

                    <TableBody>
                        {
                            rows
                                .slice(pageCurrent * rowsPerPage, pageCurrent * rowsPerPage + rowsPerPage)
                                .map((row, i) => {
                                    prepareRow(row)
                                    return (
                                        <TableRow key={i}>
                                            {
                                                row.cells.map((cell, i) => {
                                                    return <TableCell key={i}>{cell.render('Cell')}</TableCell>
                                                })
                                            }
                                        </TableRow>
                                    )  
                                })
                        }
                    </TableBody>
                </Table>
            </TableContainer>

            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={pageCurrent}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

class TableList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { dataTable, columnsTable } = this.props;

        return (
            <TableRender dataTable={dataTable} columnsTable={columnsTable} />
        );
    }
}

export default TableList;