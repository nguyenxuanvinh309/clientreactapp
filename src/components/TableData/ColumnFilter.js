import React from 'react';
import TextField from '@material-ui/core/TextField';

const ColumnFilter = ({ column }) => {
    const { filterValue, setFilter } = column;

    return (
        <TextField 
            label="Search" 
            placeholder="Search Text"
            value={filterValue || ''} 
            onChange={(e) => setFilter(e.target.value)} 
        />
    );
}

export default ColumnFilter;