import React, { Component } from 'react';
import TableList from './TableList';
import { COLUMNS } from '../../components/TableData/Columns';

import './TableData.css';

class TableData extends Component {
    constructor(props) {
        super(props);

        this.state = {
            columnsTable: COLUMNS,
            dataTable: [],
            status: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.updateStateOfComponent('dataTable', nextProps);
    }
    
    /**
     * updateStateOfComponent: Update State Of Component
     * @param {*} name: Name of State 
     * @param {*} nextProps: Next Props 
     */
    updateStateOfComponent = (name, nextProps) => {
        let newState = this.state[name];
        let status = false;

        if (this.state[name] !== nextProps[name]) {
            status = true;
            newState = nextProps[name];
        } else {
            status = false;
            newState = newState;
        } 

        if (status) this.setState({ [name]: newState, status: status });
    }

    render() {
        const { dataTable, columnsTable, status } = this.state;
                
        return (
            status && <TableList dataTable={dataTable} columnsTable={columnsTable} />
        );
    }
}

export default TableData;
