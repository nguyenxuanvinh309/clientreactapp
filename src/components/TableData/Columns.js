export const COLUMNS = [
    {
        Header: 'COUNTRY CODE',
        accessor: 'CountryCode',
        Filter: ''
    },
    {
        Header: 'COUNTRY',
        accessor: 'Country',
        Filter: ''

    },
    {
        Header: 'TOTAL CONFIRM',
        accessor: 'TotalConfirmed',
        Filter: ''

    },
    {
        Header: 'TOTAL DEASTHS',
        accessor: 'TotalDeaths',
        Filter: ''

    },
    {
        Header: 'TOTALRECOVERED',
        accessor: 'TotalRecovered',
        Filter: ''
    }
]