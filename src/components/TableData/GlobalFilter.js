import React from 'react'
import TextField from '@material-ui/core/TextField';

const GlobalFilter = ({ filter, setFilter }) => {
    return (
        <TextField 
            label="Search" 
            placeholder="Search Text"
            value={filter || ''} 
            onChange={(e) => setFilter(e.target.value)} 
        />
    );
}

export default GlobalFilter;