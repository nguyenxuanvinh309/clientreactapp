import React from 'react';
import { Grid } from '@material-ui/core';
import './Cards.css';
import Loader from '../Loader/Loader';
import CardItem from '../CardItem/CardItem';

class Cards extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { data, lastUpdate, isloaded } = this.props;

        return (
            isloaded ? (
                <div className="container-fluid">
                    <Grid container spacing={3} justify="center">
                        <CardItem 
                            data={data} 
                            lastUpdate={lastUpdate} 
                            classGridName="infected" 
                            name="Infected" 
                            nameData="TotalConfirmed"
                            description="Number of active cases from COVID-19." 
                        />
                        <CardItem 
                            data={data} 
                            lastUpdate={lastUpdate} 
                            classGridName="recovered" 
                            name="Recoveries"
                            nameData="TotalRecovered"
                            description="Number of recoveries from COVID-19." 
                        />
                        <CardItem 
                            data={data} 
                            lastUpdate={lastUpdate} 
                            classGridName="deaths" 
                            name="Deaths" 
                            nameData="TotalDeaths"
                            description="Number of deaths caused by COVID-19." 
                        />
                    </Grid>
                </div >
            ) : <Loader />
        );
    }
}

export default Cards;