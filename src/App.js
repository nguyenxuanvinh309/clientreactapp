import React from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import Layout from "./container/Layout/Layout";
import Home from "./pages/Home/Home";
import Counter from "./pages/Counter/Counter";
import FetchData from "./pages/FetchData/FetchData";
import Login from "./pages/Login/Login";
import Dashboard from "./pages/Dashboard/Dashboard";
import { getCookie } from "./helpers/cookie.js";
import { isTrue } from "./helpers/common.js";

const LoginRoute = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />
      { isTrue(getCookie('signin')) && <Route exact path="/home" component={Dashboard} />}
      <Redirect path="*" to="/" />
    </Switch>
  );
};

const DashboardRouter = () => {
  return (
    <Switch>
      <Route exact path="/home" component={Home} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route path="/counter" component={Counter} />
      <Route path="/fetch-data/:startDateIndex?" component={FetchData} />
      <Redirect path="*" to="/home" />
    </Switch>
  );
}

export default () => (
  <Layout>
    {
      isTrue(getCookie('signin')) ? (
        <DashboardRouter />
      ) : (
        <LoginRoute />
      )
    }
  </Layout>
);
